#include <iostream>

using namespace std;

bool IsPalindrome(int number);
inline int ReverseNumber(int number);

int main(int argc, char const *argv[])
{   
    int Palindrome(0);
    
    for(int i = 0; i < 1000; i++)
    {
        for(int j = 0; j < 1000; j++)
        {
            if(IsPalindrome(i*j) && (i*j) > Palindrome) Palindrome = (i*j); 
        }
    }

    cout << Palindrome;

    return 0;
}

bool IsPalindrome(int number) {

    if(number == ReverseNumber(number)) {
        return true;
    }

    return false;
}

inline int ReverseNumber(int number) {
    int reverseNumber(0);
    int moduloRest(0);

    while(number >= 1) {
        moduloRest = number % 10;
        number /= 10;
        reverseNumber *= 10;
        reverseNumber += moduloRest;
    }

    return reverseNumber;
}