#include <iostream>

using namespace std;

int main(int argc, char const *argv[])
{
    int a(1),b(1),c(2);
    int sum(0);

    while(b + a < 4000000) {
        c = a + b;
        sum += c * !(c%2); // verify if c is even // if not : return 0
        a = b;
        b = c;
    }

    cout << sum << endl;

    return 0;
}

