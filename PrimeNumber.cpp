#include <iostream>

#define st 10001

using namespace std;

bool IsPrime(int num);

int main(int argc, char const *argv[])
{
    int primeNumber = 1;
    int i = 1;

    while(i < st) {
        primeNumber += 2;
        if(IsPrime(primeNumber)) {
            i++;
        }
    }

    cout << primeNumber;

    return 0;
}

bool IsPrime(int num) {
    if(num <= 1) return false;
    else if (num == 2) return true;
    else if(!(num%2)) return false;

    int counter = 3;

    while((counter * counter) <= num) {
        if(!(num%counter)) return false;
        else counter += 2;
    }

    return true;
}