#include <iostream>

using namespace std;

int main(int argc, char const *argv[])
{
    unsigned long long number(600851475143);
    int LPF = 1;
    
    for(LPF; LPF < number; ++LPF)
    {
        if(number%LPF == 0) {
            number /= LPF;
        }
    }

    cout << LPF;

    return 0;
}
