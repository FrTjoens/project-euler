#include <iostream>

using namespace std;

int main(int argc, char const *argv[])
{
    int sumSquare(0),squareSum(0);

    for(int i = 1; i < 101; i++) {
        sumSquare += i*i;
        squareSum += i;
    }

    squareSum *= squareSum;
    
    cout << squareSum - sumSquare;

    return 0;
}
